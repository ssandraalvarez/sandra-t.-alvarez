 Hi, my name is Sandra T. Alvarez. I'm a finance specialist at [https://maybeloan.com/](https://maybeloan.com/). MaybeLoan.com, an online credit bureau in the USA is the project we're currently working on. MaybeLoan.com allows 100 people to get loans every day.
 Many people are concerned that a microfinance loan will impact their credit score. This is not entirely true. Customers can get credit from any institution of finance. All they need is to repay their loans in time.
 However, sometimes microfinance organizations forget to or fail to give information regarding closings of loans to the Bureau of Credit Histories. The loan is still outstanding to the Bureau of Credit Histories, which means that after a time, the rating points of the borrower are lowered.
 For this reason, you should ask your MFI manager whether they have received information from the Bureau of Credit Statistics about loan closings. Also, it is recommended to review your credit report within 2 weeks after closing of the loan to ensure there aren't any errors.
 Your credit history won't be affected.
 The terms of the loan are set by the agreement that is signed by the borrower after processing is completed. The most common time frame for loans online is between 5 to 30 days. However there are microfinance companies that offer loans for longer periods.
 It is important to consider all aspects that could impact the ability to pay back a loan. Some MFIs calculate penalties for late payments. You can find out about all possible penalties from your loan agreement.
 Unexpected events could prevent you from repaying your loan on time. Contact the lender that provided the loan to ask for an extension. This will allow you to obtain better terms and prevent having a bad credit record.
 How to Utilize the Microcredit Selection Service Properly
 On the website of the lender, you will find a form with a credit calculator.
 Verify that the amount you need is exactly the same as the amount in the calculator.
 After that, you'll need a quick application form with information about your passport and the number of your bank account.
 Once your application has been approved, the lender will transfer funds into your account at a bank.
 Enjoy a successful shopping experience!
